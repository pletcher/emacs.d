;;; init.el --- A basic emacs init file

(require 'package)

(add-to-list 'package-archives
             '("melpa-stable" . "https://stable.melpa.org/packages/") t)
(add-to-list 'package-archives
             '("marmalade". "http://marmalade-repo.org/packages/") t)
(add-to-list 'package-archives '("org" . "http://orgmode.org/elpa/") t)

(defvar my-packages '(auctex
                      cider
                      clojure-mode
                      find-file-in-project
                      flx-ido
                      go-mode
                      idle-highlight-mode
                      ido-ubiquitous
                      js2-mode
                      langtool
                      magit
                      markdown-mode
                      multiple-cursors
                      olivetti
                      org
                      paredit
                      rainbow-delimiters
                      rust-mode
                      scpaste
                      smartparens
                      smex
                      solarized-theme
                      sublimity
                      web-mode
                      zenburn-theme))

(package-initialize)

(dolist (p my-packages)
  (when (not (package-installed-p p))
    (package-install p)))

(when (memq window-system '(mac ns))
  (exec-path-from-shell-initialize))

(setq custom-file "~/.emacs.d/custom.el")
(if (file-exists-p custom-file)
    (load custom-file))

(setq backup-by-copying t
      backup-directory-alist '(("." . "~/.emacs.d/.saves")))

(global-auto-revert-mode 1)

(add-to-list 'exec-path "/usr/local/bin")

;;; https://github.com/bbatsov/prelude/blob/master/core/prelude-editor.el
(require 'rect)
(defadvice kill-region (before smart-cut activate compile)
  "When called interactively with no active region, kill a single line instead."
  (interactive
   (if mark-active (list (region-beginning) (region-end) rectangle-mark-mode)
     (list (line-beginning-position)
           (line-beginning-position 2)))))

;;; minimal scroll bars
(menu-bar-mode -1)
(tool-bar-mode -1)
(horizontal-scroll-bar-mode -1)

;;; line numbers
(setq line-number-mode t)
(setq column-number-mode t)
(line-number-mode t)
(column-number-mode t)

;;; match parens
(show-paren-mode t)
(setq show-paren-delay 0.0)

;;; smooth scrolling etc.
(require 'sublimity)
(require 'sublimity-scroll)
(sublimity-mode 1)

;;; apropos
(setq apropos-do-all t)

;;; no bells
(setq ring-bell-function 'ignore)

(setq ediff-window-setup-function 'ediff-setup-windows-plain
      load-prefer-newer t
      mouse-yank-at-point t)

;;; delete selection
(delete-selection-mode 1)

;;; before-save hooks
(add-hook 'before-save-hook 'whitespace-cleanup)

;;; auctex
(setq-default TeX-engine 'xetex)
(setq-default TeX-PDF-mode t)

;;; mode hooks

;;;; clojure
(add-hook 'clojure-mode-hook 'paredit-mode)

;;;; go
(setq gofmt-command "goimports")
(add-hook 'before-save-hook 'gofmt-before-save)

;;;; org
(add-hook 'org-mode-hook 'visual-line-mode)

;;; flx
(require 'flx-ido)
(ido-mode 1)
(ido-everywhere 1)
(flx-ido-mode 1)
(setq ido-enable-flex-matching t)
(setq ido-use-faces nil)

;;; make buffer names more useful
(require 'uniquify)
(setq uniquify-buffer-name-style 'forward)

;;; default cursor
(setq-default cursor-type 'bar)

;;; set default font
(add-to-list 'default-frame-alist '(font . "Hack-14"))
(set-face-attribute 'default t :font "Hack-14")

;;; keybindings

;;;; fix meta key
(setq ns-alternate-modifier 'meta)
(setq ns-right-alternate-modifier 'none)
(setq ns-command-modifier 'super)
(setq ns-right-command-modifier 'left)
(setq ns-control-modifier 'control)
(setq ns-right-control-modifier 'left)
(setq ns-function-modifier 'none)

;;;; use hippie-expand instead of dabbrev-expand
(global-set-key (kbd "M-/") 'hippie-expand)

;;;; ibuffer
(global-set-key (kbd "C-x C-b") 'ibuffer)

;;;; isearch
(global-set-key (kbd "C-s") 'isearch-forward-regexp)
(global-set-key (kbd "C-r") 'isearch-backward-regexp)
(global-set-key (kbd "C-M-s") 'isearch-forward)
(global-set-key (kbd "C-M-r") 'isearch-backward)

;;;; smex
(global-set-key (kbd "M-x") 'smex)

;;; require final newline
(setq require-final-newline t)

;;; saveplace
(save-place-mode 1)

;;; don't use tabs by default
(setq-default indent-tabs-mode nil)

;;; smartparens
(require 'smartparens-config)

;;; theme
(setq current-theme 'zenburn)

(defun turn-on-theme (theme)
  (interactive)
  (if current-theme (disable-theme current-theme))
  (setq current-theme theme)
  (load-theme theme t))

(defun turn-on-leuven ()
  (turn-on-theme 'leuven))

(defun turn-on-solarized-light ()
  (turn-on-theme 'solarized-light))

(defun turn-on-zenburn ()
  (turn-on-theme 'zenburn)
  ;;; make highlight region visible
  (set-face-attribute 'region nil :background "#666"))

(turn-on-zenburn)

;;; fly spell
(setq ispell-program-name "/usr/local/bin/aspell")

(defun flyspell-disable ()
  (flyspell-mode -1))

(defun flyspell-enable ()
  (flyspell-mode 1))

(defun flyspell-ignore-tex ()
  (interactive)
  (set (make-variable-buffer-local 'ispell-parser) 'tex))

(defun flyspell-set-parser ()
  (interactive)
  (setq ispell-parser 'tex))

(dolist (hook '(flyspell-ignore-tex
                flyspell-set-parser))
  (add-hook 'org-mode-hook hook))

(dolist (hook '(text-mode-hook))
  (add-hook hook 'flyspell-enable)
(dolist (hook '(change-log-mode-hook log-edit-mode-hook))
  (add-hook hook 'flyspell-disable)))

(setq flyspell-issue-message-flag nil)

;;; langtool
(require 'langtool)
(setq langtool-language-tool-jar "/Users/Charles/Library/Java/Extensions/LanguageTool-3.2/languagetool-commandline.jar"
      langtool-mother-tongue "en-US")

;;; multiple-cursors
(require 'multiple-cursors)

(global-set-key (kbd "C->") 'mc/mark-next-like-this)
(global-set-key (kbd "C-<") 'mc/mark-previous-like-this)
(global-set-key (kbd "C-c C-<") 'mc/mark-all-like-this)

;;; org-mode
(require 'ox-extra)
(require 'ox-latex)

(setq org-latex-default-class "bare-article")
(setq org-latex-pdf-process '("xelatex %f"))
(setq org-latex-with-hyperref nil)

(add-to-list 'org-latex-classes
             '("mla-article"
               "\\documentclass[12pt, letterpaper]{article}
\\usepackage{ifpdf}
\\usepackage{mla}
\\usepackage{setspace}
\\usepackage{fontspec}
\\usepackage{xunicode}
\\usepackage[english]{babel}
\\usepackage[autostyle]{csquotes}
\\setmainfont{Linux Libertine}
[NO-DEFAULT-PACKAGES]
[PACKAGES]"
               ("\\section{%s}" . "\\section*{%s}")
               ("\\subsection{%s}" . "\\subsection*{%s}")
               ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
               ("\\paragraph{%s}" . "\\paragraph*{%s}")
               ("\\subparagraph{%s}" . "\\subparagraph*{%s}")))

(add-to-list 'org-latex-classes
             '("bare-article"
"\\documentclass[12pt, letterpaper]{article}
\\usepackage{ifpdf}
\\usepackage{setspace}
\\usepackage{fontspec}
\\usepackage{titlesec}
\\usepackage{xunicode}
\\usepackage[english]{babel}
\\usepackage[autostyle]{csquotes}
\\usepackage[margin=1in]{geometry}
\\setmainfont{Linux Libertine}
\\titleformat{\\section}{\\bf\\small}{\\thesection\.\\hskip 6pt}{0pt}{}
\\titleformat{\\subsection}{\\bf\\small}{\\thesubsection\.\\hskip 6pt}{0pt}{}
\\doublespacing
[NO-DEFAULT-PACKAGES]
[PACKAGES]"
("\\section{%s}" . "\\section*{%s}")
("\\subsection{%s}" . "\\subsection*{%s}")
("\\subsubsection{%s}" . "\\subsubsection*{%s}")
("\\paragraph{%s}" . "\\paragraph*{%s}")
("\\subparagraph{%s}" . "\\subparagraph*{%s}")))

(ox-extras-activate '(ignore-headlines))

(defun my-mla-article-latex-filter (text backend info) text)
(add-to-list 'org-export-filter-plain-text-functions 'my-mla-article-latex-filter)

;;; prettier-js
(load-file "~/.emacs.d/prettier-js.el")
(require 'prettier-js)

(add-hook 'before-save-hook 'prettier-before-save)

;;; prog-mode
(defun enable-linum-mode ()
  (linum-mode 1))

(dolist (hook '(rainbow-delimiters-mode
                smartparens-mode
                enable-linum-mode))
  (add-hook 'prog-mode-hook hook))

;;; text-mode
(dolist (hook '(turn-on-auto-fill
                turn-on-olivetti-mode))
  (add-hook 'text-mode-hook hook))

;;; web-mode
(add-to-list 'auto-mode-alist '("\\.js\\'" . web-mode))
(add-hook 'web-mode 'electric-pair-mode)

(setq web-mode-content-types-alist
      '(("jsx" . "\\.js[x]?\\'")))
(setq web-mode-enable-auto-pairing nil)

;;; init.el ends here
